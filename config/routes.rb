Rails.application.routes.draw do
  resources :comments
  root :to => redirect('/adventures')
  resources :adventures
end
